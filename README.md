# wg-handshaker
`wg-handshaker` is a tool for performing WireGuard handshake as defined in
[WireGuard whitepaper](https://www.wireguard.com/papers/wireguard.pdf) with a given peer without
sending or accepting any more traffic to or from a peer.
Note that only initiation part is described which consists of 3 steps:
1. Send handshake initiation
1. Receive handshake response
1. Send keepalive packet

Other parts of the handshake were cut off in order to keep implementation as minimal as possible.

# Example
In the given example 2 peers will be used:
- `initiator` - `wg-handshaker`
- `target` - Linux native WireGuard peer.

Both are running on the same Linux machine. This example is used for proof of concept reasons.
`wg-handshaker` should fully work with alternative setups. In order to use any alternative setup
(different machines, different OS'es, different target node implementations, etc.), see
[WireGuard installation guide](https://www.wireguard.com/install/) and the corresponding
documentation of the chosen application to use. Then, create an alternative setup as described
below.

## Prerequisites
* Linux OS
* WireGuard present in kernel and WireGuard tools installed. See
[WireGuard installation guide](https://www.wireguard.com/install/) for your distro. In most cases
kernel module will be prebuilt.
* `sudo` setup
* `wg-handshaker` installed (`cargo install --path .`)

## 1. Key eneration
Done once per setup. For simplicity reasons, keys of both peers will be kept in a same place by
doing:
```bash
mask=$(umask)
umask 077
mkdir -p keys
wg genkey > keys/target.key
wg pubkey < keys/target.key > keys/target.pub
wg genkey > keys/initiator.key
wg pubkey < keys/initiator.key > keys/initiator.pub
umask $mask
```

## 2. Target node setup
Done once per boot.
```bash
sudo ip link add dev wg0 type wireguard
sudo wg set wg0 listen-port 51820 private-key keys/target.key peer $(cat keys/initiator.pub)
sudo ip link set wg0 up
```
 *Note: This opens UDP port on
[0.0.0.0:51820 and :::51820](https://git.zx2c4.com/wireguard-linux/tree/drivers/net/wireguard/socket.c#349)
but any unexpected packet will be dropped*

## 3. Handshake initiation with wg-handshaker
Done any time handshake is desired. This can be done with:
```bash
wg-handshaker initiate $(cat keys/initiator.key) $(cat keys/target.pub) 127.0.0.1:51820
```

Successful handshake can be verified with `wg` command:
```bash
sudo wg show wg0
```

output of it should look something like this:
```
interface: wg0
  public key: wkd9zfvD0nqheb83K533w8tsrU2WRew9Iv/3Dv5HrG4=
  private key: (hidden)
  listening port: 51820

peer: SGuHwFWheoq6dMzit47COSAwf0niZD4jlJZ/dqXzxgU=
  endpoint: 127.0.0.1:34051
  allowed ips: (none)
  latest handshake: 2 seconds ago
  transfer: 180 B received, 92 B sent
```
(check for updated `latest handshake` value)

## -1. Teardown
In order to revert any changes done in the system, execute:
```bash
sudo ip link del wg0
```
