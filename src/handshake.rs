use blake2::digest::{FixedOutput, KeyInit};
use blake2::{Blake2s256, Blake2sMac, Digest};
use generic_array::GenericArray;
use rand_core::{CryptoRngCore, OsRng};
use ring::aead::{Aad, LessSafeKey, Nonce, UnboundKey, CHACHA20_POLY1305};
use std::error::Error;
use std::io::{Error as IOError, ErrorKind};
use x25519_dalek::{PublicKey, ReusableSecret, StaticSecret};

type HmacBlake2s = hmac::SimpleHmac<Blake2s256>;

/// Handshake defines a `WireGuard` handshake
pub struct Handshake {
    static_secret: StaticSecret,
    static_public: PublicKey,
    peer_static_public: PublicKey,
    ephemeral_private: ReusableSecret,
    chaining_key: [u8; 32],
    sending_key: [u8; 32],
    peer_index: u32,
    local_index: u32,
    hash: [u8; 32],
}

/// `LABEL_MAC1` as defined in `WireGuard` whitepaper
const LABEL_MAC1: &[u8; 8] = b"mac1----";

/// HASH(CONSTRUCTION)
const INITIAL_CHAINING_KEY: [u8; 32] = [
    96, 226, 109, 174, 243, 39, 239, 192, 46, 195, 53, 226, 160, 37, 210, 208, 22, 235, 66, 6, 248,
    114, 119, 245, 45, 56, 209, 152, 139, 120, 205, 54,
];

/// HASH(initiator.chaining_key || IDENTIFIER)
const INITIAL_HASH: [u8; 32] = [
    34, 17, 179, 97, 8, 26, 197, 102, 105, 18, 67, 219, 69, 138, 213, 50, 45, 156, 108, 102, 34,
    147, 232, 183, 14, 225, 156, 101, 186, 7, 158, 243,
];

pub const HANDSHAKE_INIT_SIZE: usize = 148;
pub const HANDSHAKE_RESP_SIZE: usize = 92;
const AEAD_LEN: usize = 16;
const TAI64_BASE: u64 = (1_u64 << 62) + 37;

impl Handshake {
    #[must_use]
    /// New creates a new instance of Handshake
    pub fn new(static_secret: StaticSecret, peer_static_public: PublicKey) -> Self {
        let static_public = PublicKey::from(&static_secret);
        // TODO: avoid instanciating values with default values when those are not relevant.
        Self {
            static_secret,
            static_public,
            peer_static_public,
            ephemeral_private: ReusableSecret::random_from_rng(OsRng),
            chaining_key: [0_u8; 32],
            sending_key: [0_u8; 32],
            peer_index: 0,
            local_index: OsRng.as_rngcore().next_u32(),
            hash: [0_u8; 32],
        }
    }

    /// `format_handshake_initiation creates` an instance of `Initiation` with the calculated
    /// data filled in
    // Allow missing panics doc as function should not panic under normal circumstances
    #[allow(clippy::missing_panics_doc)]
    pub fn format_handshake_initiation(&mut self) -> Initiation {
        // initiator.chaining_key = HASH(CONSTRUCTION)
        self.chaining_key = INITIAL_CHAINING_KEY;

        // initiator.hash = HASH(HASH(initiator.chaining_key || IDENTIFIER) || responder.static_public)
        let mut hash = blake2s2(&INITIAL_HASH, self.peer_static_public.as_bytes());

        // initiator.ephemeral_private = DH_GENERATE()
        self.ephemeral_private = ReusableSecret::random_from_rng(OsRng);

        // msg.unencrypted_ephemeral = DH_PUBKEY(initiator.ephemeral_private)
        let binding = x25519_dalek::PublicKey::from(&self.ephemeral_private);
        let unencrypted_ephemeral = binding.as_bytes();

        // initiator.hash = HASH(initiator.hash || msg.unencrypted_ephemeral)
        hash = blake2s2(&hash, unencrypted_ephemeral);

        // temp = HMAC(initiator.chaining_key, msg.unencrypted_ephemeral)
        // initiator.chaining_key = HMAC(temp, 0x1)
        self.chaining_key = hmac_blake2s_32(
            &hmac_blake2s_32(&self.chaining_key, unencrypted_ephemeral),
            &[0x1],
        );

        // temp = HMAC(initiator.chaining_key, DH(initiator.ephemeral_private, responder.static_public))
        let mut temp = hmac_blake2s_32(
            &self.chaining_key,
            self.ephemeral_private
                .diffie_hellman(&self.peer_static_public)
                .as_bytes(),
        );

        // initiator.chaining_key = HMAC(temp, 0x1)
        self.chaining_key = hmac_blake2s_32(&temp, &[0x1]);

        // key = HMAC(temp, initiator.chaining_key || 0x2)
        let mut key = hmac_blake2s2_32(&temp, &self.chaining_key, &[0x2]);

        // msg.encrypted_static = AEAD(key, 0, initiator.static_public, initiator.hash)
        let mut encrypted_static = [0_u8; 32 + AEAD_LEN];
        aead_chacha20_seal(
            &mut encrypted_static,
            &key,
            self.static_public.as_bytes(),
            &hash,
        );

        // initiator.hash = HASH(initiator.hash || msg.encrypted_static)
        hash = blake2s2(&hash, &encrypted_static);

        // temp = HMAC(initiator.chaining_key, DH(initiator.static_private, responder.static_public))
        temp = hmac_blake2s_32(
            &self.chaining_key,
            self.static_secret
                .diffie_hellman(&self.peer_static_public)
                .as_bytes(),
        );

        // initiator.chaining_key = HMAC(temp, 0x1)
        self.chaining_key = hmac_blake2s_32(&temp, &[0x1]);

        // key = HMAC(temp, initiator.chaining_key || 0x2)
        key = hmac_blake2s2_32(&temp, &self.chaining_key, &[0x2]);

        // msg.encrypted_timestamp = AEAD(key, 0, TAI64N(), initiator.hash)
        let mut ext_stamp = [0_u8; 12];
        // Allow unwrap usage assuming system time always works properly and is not going packwards
        #[allow(clippy::unwrap_used)]
        let stamp = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap();

        #[allow(clippy::integer_arithmetic)]
        #[allow(clippy::arithmetic_side_effects)]
        ext_stamp[0..8].copy_from_slice(&(stamp.as_secs() + TAI64_BASE).to_be_bytes());
        ext_stamp[8..12].copy_from_slice(&stamp.subsec_nanos().to_be_bytes());

        let mut encrypted_timestamp = [0_u8; 12 + AEAD_LEN];
        aead_chacha20_seal(&mut encrypted_timestamp, &key, &ext_stamp, &hash);

        // initiator.hash = HASH(initiator.hash || msg.encrypted_timestamp)
        self.hash = blake2s2(&hash, &encrypted_timestamp);

        // msg.mac1 = MAC(HASH(LABEL_MAC1 || responder.static_public), msg[0:offsetof(msg.mac1)])
        // TODO: Avoid creating 2 instances of `Initiation`
        let initiation = Initiation {
            sender_index: self.local_index,
            unencrypted_ephemeral: *unencrypted_ephemeral,
            encrypted_static,
            encrypted_timestamp,
            mac1: [0_u8; 16],
            mac2: [0_u8; 16],
        };

        let initiation_bytes: [u8; HANDSHAKE_INIT_SIZE] = initiation.into();

        #[allow(clippy::integer_arithmetic)]
        #[allow(clippy::arithmetic_side_effects)]
        let mac1 = keyed_blake2s_16(
            blake2s2(LABEL_MAC1, self.peer_static_public.as_bytes()),
            &initiation_bytes[..(HANDSHAKE_INIT_SIZE - 32)],
        );

        // mac2 is not needed as only handshake is supported
        // if (initiator.last_received_cookie is empty or expired)
        //     msg.mac2 = [zeros]
        // else
        //     msg.mac2 = MAC(initiator.last_received_cookie, msg[0:offsetof(msg.mac2)])
        Initiation {
            // Not important in this context
            // msg.sender_index = little_endian(initiator.sender_index)
            sender_index: self.local_index,
            unencrypted_ephemeral: *unencrypted_ephemeral,
            encrypted_static,
            encrypted_timestamp,
            mac1,
            mac2: [0_u8; 16],
        }
    }

    pub fn receive_handshake_response(
        &mut self,
        response: &Response,
    ) -> Result<(), Box<dyn Error>> {
        let resp_bytes: [u8; HANDSHAKE_RESP_SIZE] = (*response).into();
        // Verify the response
        // msg.mac1 = MAC(HASH(LABEL_MAC1 || initiator.static_public), msg[0:offsetof(msg.mac1)])
        if response.receiver_index != self.local_index
            || response.mac1
                != keyed_blake2s_16(
                    blake2s2(LABEL_MAC1, self.static_public.as_bytes()),
                    &resp_bytes[..HANDSHAKE_RESP_SIZE - 32],
                )
            || response.mac2 != [0_u8; 16]
        {
            IOError::new(ErrorKind::InvalidData, "invalid handshake response");
        }
        let unencrypted_ephemeral = PublicKey::from(response.unencrypted_ephemeral);
        // msg.unencrypted_ephemeral = DH_PUBKEY(responder.ephemeral_private)
        // responder.hash = HASH(responder.hash || msg.unencrypted_ephemeral)
        //
        // temp = HMAC(responder.chaining_key, msg.unencrypted_ephemeral)
        let mut temp = hmac_blake2s_32(&self.chaining_key, unencrypted_ephemeral.as_bytes());
        // responder.chaining_key = HMAC(temp, 0x1)
        self.chaining_key = hmac_blake2s_32(&temp, &[0x1]);
        // temp = HMAC(responder.chaining_key, DH(responder.ephemeral_private, initiator.ephemeral_public))
        let ephemeral_shared = self
            .ephemeral_private
            .diffie_hellman(&unencrypted_ephemeral);

        temp = hmac_blake2s_32(&self.chaining_key, &ephemeral_shared.to_bytes());
        // responder.chaining_key = HMAC(temp, 0x1)
        self.chaining_key = hmac_blake2s_32(&temp, &[0x1]);
        // temp = HMAC(responder.chaining_key, DH(responder.ephemeral_private, initiator.static_public))
        temp = hmac_blake2s_32(
            &self.chaining_key,
            &self
                .static_secret
                .diffie_hellman(&unencrypted_ephemeral)
                .to_bytes(),
        );
        // responder.chaining_key = HMAC(temp, 0x1)
        self.chaining_key = hmac_blake2s_32(&temp, &[0x1]);
        // temp = HMAC(responder.chaining_key, preshared_key)
        // preshared_key is optional, therefore use empty here
        temp = hmac_blake2s_32(&self.chaining_key, &[0_u8; 32]);
        // responder.chaining_key = HMAC(temp, 0x1)
        self.chaining_key = hmac_blake2s_32(&temp, &[0x1]);

        // Not needed
        // temp2 = HMAC(temp, responder.chaining_key || 0x2)
        // key = HMAC(temp, temp2 || 0x3)
        // responder.hash = HASH(responder.hash || temp2)
        // msg.encrypted_nothing = AEAD(key, 0, [empty], responder.hash)
        // responder.hash = HASH(responder.hash || msg.encrypted_nothing)
        //

        // Calculated during validation
        // msg.mac1 = MAC(HASH(LABEL_MAC1 || initiator.static_public), msg[0:offsetof(msg.mac1)])
        // if (responder.last_received_cookie is empty or expired)
        //     msg.mac2 = [zeros]
        // else
        //     msg.mac2 = MAC(responder.last_received_cookie, msg[0:offsetof(msg.mac2)])

        // receiving_key is not needed as only initiator is supported
        // counters are not needed as only handshake is supported
        // temp1 = HMAC(initiator.chaining_key, [empty])
        // temp2 = HMAC(temp1, 0x1)
        // temp3 = HMAC(temp1, temp2 || 0x2)
        // initiator.sending_key = temp2
        // initiator.receiving_key = temp3
        // initiator.sending_key_counter = 0
        // initiator.receiving_key_counter = 0

        self.sending_key = hmac_blake2s_32(&hmac_blake2s_32(&self.chaining_key, &[]), &[0x1]);
        self.peer_index = response.sender_index;
        Ok(())
    }

    pub fn format_keepalive(&mut self) -> PacketData {
        let mut encrypted_encapsulated_packet: [u8; AEAD_LEN] = [0_u8; AEAD_LEN];

        aead_chacha20_seal(
            &mut encrypted_encapsulated_packet,
            &self.sending_key,
            &[],
            &[],
        );

        PacketData {
            receiver_index: self.peer_index,
            counter: 0,
            encrypted_encapsulated_packet,
        }
    }
}

/// `Initiation` defines the Initiator to Responder message as described in
/// <https://www.wireguard.com/protocol/>
pub struct Initiation {
    sender_index: u32,
    unencrypted_ephemeral: [u8; 32],
    encrypted_static: [u8; 32 + AEAD_LEN],
    encrypted_timestamp: [u8; 12 + AEAD_LEN],
    mac1: [u8; 16],
    mac2: [u8; 16],
}

// TODO: optimize
impl From<Initiation> for [u8; HANDSHAKE_INIT_SIZE] {
    fn from(val: Initiation) -> Self {
        let mut ret = [0_u8; HANDSHAKE_INIT_SIZE];
        ret[0] = 1;
        // bytes 1..3 are reserved zeros
        ret[4..8].copy_from_slice(&val.sender_index.to_le_bytes());
        ret[8..40].copy_from_slice(&val.unencrypted_ephemeral);
        ret[40..88].copy_from_slice(&val.encrypted_static);
        ret[88..116].copy_from_slice(&val.encrypted_timestamp);
        ret[116..132].copy_from_slice(&val.mac1);
        ret[132..HANDSHAKE_INIT_SIZE].copy_from_slice(&val.mac2);
        ret
    }
}

#[derive(Copy, Clone, Debug)]
/// `Response` defines the Responder to Initiator message as described in
/// <https://www.wireguard.com/protocol/>
pub struct Response {
    sender_index: u32,
    receiver_index: u32,
    unencrypted_ephemeral: [u8; 32],
    encrypted_nothing: [u8; AEAD_LEN],
    mac1: [u8; 16],
    mac2: [u8; 16],
}

// TODO: optimize
impl TryFrom<[u8; HANDSHAKE_RESP_SIZE]> for Response {
    type Error = core::array::TryFromSliceError;

    fn try_from(data: [u8; HANDSHAKE_RESP_SIZE]) -> Result<Self, Self::Error> {
        Ok(Self {
            // bytes 1..3 are reserved zeros
            sender_index: u32::from_le_bytes(data[4..8].try_into()?),
            receiver_index: u32::from_le_bytes(data[8..12].try_into()?),
            unencrypted_ephemeral: data[12..44].try_into()?,
            encrypted_nothing: data[44..60].try_into()?,
            mac1: data[60..76].try_into()?,
            mac2: data[76..92].try_into()?,
        })
    }
}

// TODO: optimize
impl From<Response> for [u8; HANDSHAKE_RESP_SIZE] {
    fn from(val: Response) -> [u8; HANDSHAKE_RESP_SIZE] {
        let mut ret = [0_u8; HANDSHAKE_RESP_SIZE];
        ret[0] = 2;
        // bytes 1..3 are reserved zeros
        ret[4..8].copy_from_slice(&val.sender_index.to_le_bytes());
        ret[8..12].copy_from_slice(&val.receiver_index.to_le_bytes());
        ret[12..44].copy_from_slice(&val.unencrypted_ephemeral);
        ret[44..60].copy_from_slice(&val.encrypted_nothing);
        ret[60..76].copy_from_slice(&val.mac1);
        ret[76..92].copy_from_slice(&val.mac2);
        ret
    }
}

/// `PacketData` defines the Exchange of Data Packets as described in
/// <https://www.wireguard.com/protocol/>
pub struct PacketData {
    receiver_index: u32,
    counter: u64,
    encrypted_encapsulated_packet: [u8; AEAD_LEN],
}

// TODO: optimize
impl From<PacketData> for [u8; 32] {
    fn from(val: PacketData) -> Self {
        let mut ret = [0_u8; 32];
        ret[0] = 4;
        // bytes 1..3 are reserved zeros
        ret[4..8].copy_from_slice(&val.receiver_index.to_le_bytes());
        ret[8..16].copy_from_slice(&val.counter.to_le_bytes());
        ret[16..32].copy_from_slice(&val.encrypted_encapsulated_packet);
        ret
    }
}

/// blake2s2 does the same as blake2s but is used for:
/// HASH(input1 || input2)
fn blake2s2(input1: &[u8], input2: &[u8]) -> [u8; 32] {
    let mut hasher = Blake2s256::new();
    hasher.update(input1);
    hasher.update(input2);
    hasher.finalize().into()
}

/// `hmac_blake2s` creates a HMAC for given key and data using Blake2s256 hash, representing:
/// HMAC(key, input): HMAC-Blake2s(key, input, 32), returning 32 bytes of output
fn hmac_blake2s_32(key: &[u8; 32], input: &[u8]) -> [u8; 32] {
    use blake2::digest::Update;
    // Allow unwrap used as key length is always 32 bytes
    #[allow(clippy::unwrap_used)]
    let mut hmac = HmacBlake2s::new_from_slice(key).unwrap();
    hmac.update(input);
    hmac.finalize_fixed().into()
}

/// `hmac_blake2s2_32` does the same as hmac2  but is used for:
/// HMAC(key, input1 || input2)
pub(crate) fn hmac_blake2s2_32(key: &[u8; 32], input1: &[u8], input2: &[u8]) -> [u8; 32] {
    use blake2::digest::Update;
    #[allow(clippy::missing_docs_in_private_items)]
    type HmacBlake2s = hmac::SimpleHmac<Blake2s256>;
    // Allow unwrap used as key length is always 32 bytes
    #[allow(clippy::unwrap_used)]
    let mut hmac = HmacBlake2s::new_from_slice(key).unwrap();
    hmac.update(input1);
    hmac.update(input2);
    hmac.finalize_fixed().into()
}

/// AEAD(key, counter, plain text, auth text): `ChaCha20Poly1305` AEAD, as specified in RFC7539
fn aead_chacha20_seal(ciphertext: &mut [u8], key: &[u8; 32], data: &[u8], aad: &[u8]) {
    // Allow unwrap used as key length is always 32 bytes #[allow(clippy::unwrap_used)]
    let key = LessSafeKey::new(UnboundKey::new(&CHACHA20_POLY1305, key).unwrap());
    // Allow indexing slicing as function is private, therefore all callers are expected to provide
    // ciphertext which has length of data.len() + AEAD_LEN
    #[allow(clippy::indexing_slicing)]
    ciphertext[..data.len()].copy_from_slice(data);

    // Should never fail within this function
    #[allow(clippy::unwrap_used)]
    let tag = key
        .seal_in_place_separate_tag(
            // From docs:
            // with its nonce being composed of 32 bits of zeros followed by the 64-bit little-endian value of counter
            // Counter is only used for data packets which are not relevant in handshake
            Nonce::assume_unique_for_key([0; 12]),
            Aad::from(aad),
            #[allow(clippy::indexing_slicing)]
            &mut ciphertext[..data.len()],
        )
        .unwrap();

    #[allow(clippy::indexing_slicing)]
    ciphertext[data.len()..].copy_from_slice(tag.as_ref());
}

/// MAC(key, input): Keyed-Blake2s(key, input, 16), returning 16 bytes of output
fn keyed_blake2s_16(key: [u8; 32], data1: &[u8]) -> [u8; 16] {
    let mut hmac = Blake2sMac::new(GenericArray::from_slice(&key));
    blake2::digest::Update::update(&mut hmac, data1);
    hmac.finalize_fixed().into()
}

#[cfg(test)]
mod test {
    use super::*;

    /// `IDENTIFIER` as defined in `WireGuard` whitepaper
    const IDENTIFIER: &[u8; 34] = b"WireGuard v1 zx2c4 Jason@zx2c4.com";

    #[test]
    fn test_blake2s2() {
        assert_eq!(INITIAL_HASH, blake2s2(&INITIAL_CHAINING_KEY, IDENTIFIER));
    }

    #[test]
    fn test_hmac_blake2s_32() {
        assert_eq!(
            [
                171, 181, 96, 32, 216, 8, 174, 210, 163, 228, 242, 1, 40, 58, 243, 50, 22, 255, 63,
                126, 45, 231, 99, 185, 40, 220, 234, 28, 251, 192, 40, 239
            ],
            hmac_blake2s_32(&INITIAL_CHAINING_KEY, &[0x1])
        );
        assert_eq!(
            [
                209, 60, 113, 6, 143, 9, 225, 115, 238, 216, 132, 137, 85, 108, 82, 22, 189, 28,
                227, 195, 33, 74, 126, 227, 14, 3, 44, 245, 39, 244, 45, 241
            ],
            hmac_blake2s_32(&INITIAL_CHAINING_KEY, &[0x2])
        );
    }

    #[test]
    fn test_hmac_blake2s2_32() {
        assert_eq!(
            [
                27, 107, 240, 85, 73, 226, 133, 40, 186, 213, 74, 156, 139, 235, 142, 112, 135, 13,
                121, 19, 173, 11, 183, 80, 216, 106, 51, 90, 126, 77, 232, 6
            ],
            hmac_blake2s2_32(&INITIAL_CHAINING_KEY, &[0x1], &[0x2])
        );
    }

    #[test]
    fn test_keyed_blake2s_16() {
        assert_eq!(
            [118, 234, 222, 31, 252, 121, 79, 228, 126, 40, 255, 79, 16, 43, 213, 112],
            keyed_blake2s_16(INITIAL_CHAINING_KEY, &[0x1])
        );
    }

    #[test]
    fn test_aead_chacha20_seal() {
        let key = INITIAL_CHAINING_KEY;
        let data = [1, 2, 3];
        let aad = [3, 2, 1];
        let mut ciphertext = vec![0_u8; data.len() + AEAD_LEN];
        aead_chacha20_seal(&mut ciphertext, &key, &data, &aad);

        use ring::aead::*;
        let key = LessSafeKey::new(UnboundKey::new(&CHACHA20_POLY1305, &key).unwrap());
        let plaintext = key
            .open_in_place(
                Nonce::assume_unique_for_key([0; 12]),
                Aad::from(&aad),
                &mut ciphertext,
            )
            .unwrap();
        assert_eq!(data, plaintext);
    }
}
