//! Usage: wg-handshaker <COMMAND>

use base64::engine::general_purpose::STANDARD as STANDARD_DECODER;
use base64::Engine;
use clap::{Parser, Subcommand};
use std::error::Error;
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
use tokio::net::UdpSocket;
use tokio::time::{timeout, Duration};

pub mod handshake;

// `RECV_TIMEOUT_SECS` defines the timeout for receiving data to UDP socket in seconds
const RECV_TIMEOUT_SECS: u64 = 1;

#[derive(Parser)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Initiates handshake with a given `WireGuard` peer
    Initiate {
        private_key: String,
        peer_public_key: String,
        peer_endpoint: std::net::SocketAddr,
    },
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let cli = Cli::parse();
    match cli.command {
        Commands::Initiate {
            private_key,
            peer_public_key,
            peer_endpoint,
        } => initiate_handshake(private_key, peer_public_key, peer_endpoint).await,
    }
}

async fn initiate_handshake(
    private_key_str: String,
    peer_public_key_str: String,
    peer_endpoint: SocketAddr,
) -> Result<(), Box<dyn Error>> {
    // Parse input
    let decoder = STANDARD_DECODER;
    let static_secret: [u8; 32] = decoder.decode(private_key_str)?.as_slice().try_into()?;

    let peer_public: [u8; 32] = decoder.decode(peer_public_key_str)?.as_slice().try_into()?;
    let mut hs = handshake::Handshake::new(
        x25519_dalek::StaticSecret::from(static_secret),
        x25519_dalek::PublicKey::from(peer_public),
    );

    // Create socket
    let socket = UdpSocket::bind(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, 0)).await?;
    socket.connect(peer_endpoint).await?;

    // Send handshake initiation
    let initiation: [u8; handshake::HANDSHAKE_INIT_SIZE] = hs.format_handshake_initiation().into();
    socket.send(&initiation).await?;

    // Receive handshake response
    let mut handshake_response_bytes = [0_u8; handshake::HANDSHAKE_RESP_SIZE];
    timeout(
        Duration::from_secs(RECV_TIMEOUT_SECS),
        socket.recv(&mut handshake_response_bytes),
    )
    .await??;
    let resp = handshake::Response::try_from(handshake_response_bytes)?;
    hs.receive_handshake_response(&resp)?;

    // Send keepalive packet
    let packet_data: [u8; 32] = hs.format_keepalive().into();
    socket.send(&packet_data).await?;
    Ok(())
}
